# utils.py
import re
import requests
from threading import Thread
from gpiozero import LED
from time import sleep
import os
from config import SERVER_URL, ID_QR, LED_PIN, LOG_DIARIO, TOKEN_LOGNAME
from logger_config import logger, token_logger
from client import conectar_y_escuchar

def es_url(cad):
    regex_url = r"https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+"
    urls_encontradas = re.findall(regex_url, cad)
    return bool(urls_encontradas)

def abrir_puerta(response_text):
    message = "Abriendo puertas"
    logger.info(message)
    print(message)
    led = LED(LED_PIN)
    if "Salida registrada con éxito" in response_text: #Validacion para abrir puerta
        message = "Led verde"
        logger.info(message)
        print(message)
        led.off()
        sleep(1)
        led.on()
# Evita repetir la lectura.
def token_ya_leido(token):
    token_log_path = os.path.join(LOG_DIARIO, TOKEN_LOGNAME)
    if not os.path.exists(token_log_path):
        return False
    with open(token_log_path, 'r', encoding='utf-8') as f:
        for line in f:
            if token in line:
                return True
    return False

def peticion_back(cad):
    message = "--Peticion al back--"
    logger.info(message)
    print(message)
    if es_url(cad):
        message = "La cadena es una URL"
        logger.info(message)
        print(message)
        myobj = {'ID_QR': ID_QR}
        #message = "Iniciando QR"
        logger.info(message)
        print(message)
        red = re.split(r'(validate|registry_entry)', cad)

        if len(red) == 3:
            try:
                token = red[2].strip().replace("/", "")
                print(token)
                logger.info(f"{token}")
                if token_ya_leido(token):
                    message = "Token ya ha sido leido"
                    logger.warning(message)
                    print(message)
                    return
                token_logger.info(token)  # Log the token
                data_to_send = token
                conectar_y_escuchar(data_to_send)
                mytoken = {'code': f'{token}'}
                data = {**myobj, **mytoken}
                url = f"https://{SERVER_URL}/qrcode/qr/registry_entry/"
                response = requests.post(url, data=data, headers={'Version': 'v3'})
                message = f"Peticion a {url} con status {response.status_code}"
                logger.info(message)
                print(message)
                message = f"response.text {response.text}"
                logger.info(message)
                print(message)
                abrir_puerta(response.text)
            except Exception as e:
                message = f"Error: {str(e)}"
                logger.error(message)
                print(message)
        else:
            message = "Cadena no contiene 'validate' o 'registry_entry'"
            logger.warning(message)
            print(message)
    else:
        message = "La cadena es un token"
        logger.info(message)
        print(message)
        print(cad)
        logger.error(f'error {cad}')
        if len(cad) == 15:
            myobj = {'ID_QR': ID_QR}
            token = cad
            logger.info(f"{token}")
            if token_ya_leido(token):
                message = "Token ya ha sido leido"
                logger.warning(message)
                print(message)
                return
            token_logger.info(token)  # Log the token
            data_to_send = token
            conectar_y_escuchar(data_to_send)
            print(f" antes del my token {token}")
            mytoken = {'code': f'{token}'}
            data = {**myobj, **mytoken}
            url = f"https://{SERVER_URL}/qrcode/qr/registry_entry/"
            response = requests.post(url, data=data, headers={'Version': 'v3'})
            message = f"Peticion a {url} con status {response.status_code}"
            logger.info(message)
            print(message)
            message = f"response.text {response.text}"
            logger.info(message)
            print(message)
            abrir_puerta(response.text)
        else:
            message = "La cadena no tiene la longitud correcta"
            logger.warning(message)
            print(message)
