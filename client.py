#client.py
import socket

SERVER_IP = ''  # Reemplaza con la IP de tu servidor(El servidro se levanta desde el codigo del qr de entrada)
SERVER_PORT = 65432 # Reemplaza con el puerto de tu servidor

def conectar_y_escuchar(data):
    client_socket = None
    try:
        # Crear un socket TCP/IP
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        # Conectar al servidor
        client_socket.connect((SERVER_IP, SERVER_PORT))
        
        # Enviar datos al servidor
        client_socket.sendall(data.encode())
        
        # Recibir respuesta del servidor
        respuesta = client_socket.recv(1024)
        print(f"Respuesta del servidor: {respuesta.decode()}")
        
    except Exception as e:
        print(f"Error al conectar con el servidor: {e}")
        
    finally:
        if client_socket:
            # Cerrar el socket si esta definido
            client_socket.close()

if __name__ == "__main__":
    # Ejemplo de envio de datos al servidor
    conectar_y_escuchar(data_to_send)
