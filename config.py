# config.py
import os

LOG_DIARIO = os.path.expanduser("~/Desktop/logs_QR")  # Ubicacion del archivo Logs
LOGNAME = "log_QR.log"  # Nombre del archivo
TOKEN_LOGNAME = "token.log"  # Nombre del archivo de tokens
SERVER_URL = 'api.micasitaapp.com'  # Servidor al que se envia la peticion 
LED_PIN = 21 # PIN para abrir puerta
DEVICE_NAME = 'Linux 3.4.39 with sunxi_usb_udc HID Gadget'  # Reemplazar con el nombre del dispositivo de entrada correcto
ID_QR = ''  # ID del QR
QR_EXPIRATION_TIME = 5  # tiempo en segundos para permitir la revalidacion del mismo QR
